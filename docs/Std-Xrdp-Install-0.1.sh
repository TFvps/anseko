################################################################
# Script_Name : xrdp-std-install-0.1.sh
# Description : Perform an automated standard installation of xrdp
# on ubuntu 17.10
# Date : November 2017
# written by : Griffon
# Web Site :http://www.c-nergy.be - http://www.c-nergy.be/blog
# Version : 0.1
# Disclaimer : Script provided AS IS. Use it at your own risk....
##################################################################

#---------------------------------------------------#
# Step 0 - Try to Detect Ubuntu Version.... 
#---------------------------------------------------#

version=$(lsb_release -d | awk -F":" '/Description/ {print $2}') 

if [ "$version" = "	Ubuntu 17.10" ]
then
echo
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
/bin/echo -e "\e[1;32mSupported version detected....proceeding\e[0m"
/bin/echo -e "\e[1;32mUbuntu Version :$version\e[0m"
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"

else
/bin/echo -e "\e[1;31m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
/bin/echo -e "\e[1;31mYour system is not running Ubuntu 17.10 Edition.\e[0m"
/bin/echo -e "\e[1;31mThe script has been tested only on Ubuntu 17.10...\e[0m"
/bin/echo -e "\e[1;31mThe script is exiting...\e[0m"
/bin/echo -e "\e[1;31m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
echo
exit
fi

#---------------------------------------------------#
# Step 1 - Install xRDP Software.... 
#---------------------------------------------------#
echo
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
/bin/echo -e "\e[1;32mInstalling xrdp package....proceeding\e[0m"
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
echo
sudo apt-get install xrdp -y 

#---------------------------------------------------#
# Step 2 - Install Gnome Tweak Tool.... 
#---------------------------------------------------#
echo
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
/bin/echo -e "\e[1;32mInstalling Gnome tweak package....proceeding\e[0m"
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
echo
sudo apt-get install gnome-tweak-tool -y

#---------------------------------------------------#
# Step 3 - Allow console Access .... 
#---------------------------------------------------#
echo
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
/bin/echo -e "\e[1;32mGranting Console Access....proceeding\e[0m"
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
echo
sudo sed -i 's/allowed_users=console/allowed_users=anybody/' /etc/X11/Xwrapper.config

#---------------------------------------------------#
# Step 4 - create policies exceptions .... 
#---------------------------------------------------#
echo
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
/bin/echo -e "\e[1;32mCreating Polkit file....proceeding\e[0m"
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
echo
sudo bash -c 'cat >/etc/polkit-1/localauthority.conf.d/02-allow-colord.conf <<EOF
polkit.addRule(function(action, subject) {
if ((action.id == “org.freedesktop.color-manager.create-device” ||
action.id == “org.freedesktop.color-manager.create-profile” ||
action.id == “org.freedesktop.color-manager.delete-device” ||
action.id == “org.freedesktop.color-manager.delete-profile” ||
action.id == “org.freedesktop.color-manager.modify-device” ||
action.id == “org.freedesktop.color-manager.modify-profile”) &&
subject.isInGroup(“{group}”)) {
return polkit.Result.YES;
}
});
EOF'

#---------------------------------------------------#
# Step 5 - Enable Extensions .... 
#---------------------------------------------------#
echo
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
/bin/echo -e "\e[1;32mEnabling Extensions....proceeding\e[0m"
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
echo
gnome-shell-extension-tool -e ubuntu-dock@ubuntu.com
gnome-shell-extension-tool -e ubuntu-appindicators@ubuntu.com

#---------------------------------------------------#
# Step 6 - Credits .... 
#---------------------------------------------------#

/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
/bin/echo -e "\e[1;32mInstallation Completed\e[0m"
/bin/echo -e "\e[1;32mPlease test your xRDP configuration....\e[0m"
/bin/echo -e "\e[1;32mwritten by Griffon - November 2017 - Version 0.1 - xrdp-std-install-0.1.sh\e[0m"
/bin/echo -e "\e[1;32m!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\e[0m"
echo



