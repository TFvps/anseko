@echo off
@echo -----------------------------------------------------------
@echo ------ ENG Binary Android ROOT Script               -------
@echo ------ SuperSU version: 2.82                        -------
@echo ------ For MSM8998 / by bingoCoder2013 / 2018-5-8   -------
@echo -----------------------------------------------------------

::---------------------------------------------------------------
::------- Define related folder & files names here --------------
::---------------------------------------------------------------
set CPU_TYPE=arm64
set LIB_FORDER=lib64

::set CPU_TYPE=armv7
::set LIB_FORDER=lib
::---------------------------------------------------------------
::---------------------- Define End -----------------------------
::---------------------------------------------------------------
adb kill-server
adb start-server
adb root
adb remount
adb shell setenforce 0
adb push common/Superuser.apk /system/app/SuperSU/SuperSU.apk 
adb shell chmod 0644 /system/app/SuperSU/SuperSU.apk 
adb shell chcon u:object_r:system_file:s0 /system/app/SuperSU/SuperSU.apk 

::add running su deamon to install-recovery.sh
adb shell "echo >> /system/bin/install-recovery.sh"
adb shell "echo '# Disable SELINUX & Run SuperSU deamon' >> /system/bin/install-recovery.sh"
adb shell "echo '/system/xbin/daemonsu --auto-daemon &' >> /system/bin/install-recovery.sh"
adb shell chmod 0755 /system/bin/install-recovery.sh

adb push %CPU_TYPE%/su /system/xbin/su 
adb shell chmod 0755 /system/xbin/su 
adb shell chcon u:object_r:system_file:s0 /system/xbin/su 

adb push %CPU_TYPE%/su /system/bin/.ext/.su 
adb shell chmod 0755 /system/bin/.ext/.su 
adb shell chcon u:object_r:system_file:s0 /system/bin/.ext/.su 

adb push %CPU_TYPE%/su /system/xbin/daemonsu 
adb shell chmod 0755 /system/xbin/daemonsu 
adb shell chcon u:object_r:system_file:s0 /system/xbin/daemonsu 

adb push %CPU_TYPE%/supolicy /system/xbin/supolicy 
adb shell chmod 0755 /system/xbin/supolicy 
adb shell chcon u:object_r:system_file:s0 /system/xbin/supolicy 

adb push %CPU_TYPE%/libsupol.so /system/%LIB_FORDER%/libsupol.so 
adb shell chmod 0755 /system/%LIB_FORDER%/libsupol.so 
adb shell chcon u:object_r:system_file:s0 /system/%LIB_FORDER%/libsupol.so 

::adb shell su --install
@echo -----------------------------------------------------------
@echo All done! 
@echo Press any key to reboot ...
@echo If you do not want to reboot, plz close this CMD window
@echo -----------------------------------------------------------
pause
adb reboot