package org.i9.slb.platform.anseko.libs.net.rfb;

public abstract class rfb {

    public static final String ProtocolVersionMsg = "RFB 003.003\n";
    public static final int ConnFailed = 0;
    public static final int NoAuth = 1;
    public static final int VncAuth = 2;
    public static final int VncAuthOK = 0;
    public static final int VncAuthFailed = 1;
    public static final int VncAuthTooMany = 2;
    public static final int FrameBufferUpdate = 0;
    public static final int SetColourMapEntries = 1;
    public static final int Bell = 2;
    public static final int ServerCutText = 3;
    public static final int SetPixelFormat = 0;
    public static final int FixColourMapEntries = 1;
    public static final int SetEncodings = 2;
    public static final int FrameBufferUpdateRequest = 3;
    public static final int KeyEvent = 4;
    public static final int PointerEvent = 5;
    public static final int ClientCutText = 6;
    public static final int Button1Mask = 1;
    public static final int Button2Mask = 2;
    public static final int Button3Mask = 4;
    public static final int EncodingRaw = 0;
    public static final int EncodingCopyRect = 1;
    public static final int EncodingRRE = 2;
    public static final int EncodingCoRRE = 4;
    public static final int EncodingHextile = 5;
    public static final int HextileRaw = 1;
    public static final int HextileBackgroundSpecified = 2;
    public static final int HextileForegroundSpecified = 4;
    public static final int HextileAnySubrects = 8;
    public static final int HextileSubrectsColoured = 16;

    public rfb() {
    }

    public static int swapShort(int i) {
        return (i & 0xff) << 8 | i >> 8 & 0xff;
    }

    public static int swapInt(int i) {
        return (i & 0xff000000) >> 24 | (i & 0xff0000) >> 8 | (i & 0xff00) << 8 | (i & 0xff) << 24;
    }
}
