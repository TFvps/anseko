package org.i9.slb.platform.anseko.libs.net.rfb.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb.server:
//			RFBSocket, RFBClient

public interface RFBAuthenticator {

    public abstract boolean authenticate(DataInputStream datainputstream, DataOutputStream dataoutputstream, RFBSocket rfbsocket)
            throws IOException;

    public abstract int getAuthScheme(RFBClient rfbclient);
}
