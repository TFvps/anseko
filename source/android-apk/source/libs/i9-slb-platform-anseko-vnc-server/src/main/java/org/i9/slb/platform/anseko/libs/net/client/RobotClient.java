package org.i9.slb.platform.anseko.libs.net.client;

import org.i9.slb.platform.anseko.libs.net.rfb.Colour;
import org.i9.slb.platform.anseko.libs.net.rfb.PixelFormat;
import org.i9.slb.platform.anseko.libs.net.rfb.Rect;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

public class RobotClient {

    public static final int CELLS = 4;
    protected int pe;
    protected PixelFormat pf;
    protected int no;
    protected Colour colourMap[];
    protected BufferedImage oldImage;
    protected int index;
    protected Rectangle rects[];

    public RobotClient() {
        index = 0;
        rects = new Rectangle[16];
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++)
                rects[i * 4 + j] = new Rectangle();

        }

    }

    protected Rect[] getChangedImages(BufferedImage bufferedimage) {
        int i = bufferedimage.getWidth();
        int j = bufferedimage.getHeight();
        if (oldImage == null || oldImage.getWidth() != i || oldImage.getHeight() != j) {
            index = 0;
            int k = i / 4;
            int l = j / 4;
            for (int j1 = 0; j1 < 4; j1++) {
                for (int k1 = 0; k1 < 4; k1++) {
                    rects[j1 * 4 + k1].x = k * j1;
                    rects[j1 * 4 + k1].y = l * k1;
                    rects[j1 * 4 + k1].width = k;
                    rects[j1 * 4 + k1].height = l;
                }

            }

            Rect rect = Rect.encode(pe, pf, bufferedimage, 0, 0);
            oldImage = bufferedimage;
            return (new Rect[]{
                    rect
            });
        }
        ArrayList arraylist = new ArrayList();
        for (int i1 = 0; i1 < 16; i1++) {
            Rect rect1 = getChangedImages(bufferedimage, i1);
            if (rect1 != null)
                arraylist.add(rect1);
        }

        oldImage = bufferedimage;
        if (arraylist.size() > 0) {
            Rect arect[] = new Rect[0];
            arect = (Rect[]) arraylist.toArray(arect);
            return arect;
        } else {
            return null;
        }
    }

    private Rect getChangedImages(BufferedImage paramBufferedImage, int paramInt) {
        Rectangle localRectangle = this.rects[paramInt];
        int i = 0;
        int j = 0;
        int k = localRectangle.x;
        int m = localRectangle.y;
        int n = localRectangle.x + localRectangle.width;
        int i1 = localRectangle.y + localRectangle.height;
        try {
            for (i = k; i < n; i++)
                for (j = m; j < i1; j++) {
                    if (this.oldImage.getRGB(i, j) == paramBufferedImage.getRGB(i, j))
                        continue;
                    throw new Exception();
                }
            return null;
        } catch (Exception e1) {
            k = i;
            try {
                for (j = m; j < i1; j++)
                    for (i = k; i < n; i++) {
                        if (this.oldImage.getRGB(i, j) == paramBufferedImage.getRGB(i, j))
                            continue;
                        throw new Exception();
                    }
                return null;
            } catch (Exception e2) {
                m = j;
                try {
                    for (i = n - 1; i > k; i--)
                        for (j = m; j < i1; j++) {
                            if (this.oldImage.getRGB(i, j) == paramBufferedImage.getRGB(i, j))
                                continue;
                            throw new Exception();
                        }
                    return null;
                } catch (Exception e3) {
                    n = i;
                    try {
                        for (j = i1 - 1; j > m; j--)
                            for (i = n; i > k; i--) {
                                if (this.oldImage.getRGB(i, j) == paramBufferedImage.getRGB(i, j))
                                    continue;
                                throw new Exception();
                            }
                        return null;
                    } catch (Exception e4) {
                        i1 = j;
                        if ((n - k > 0) && (i1 - m > 0)) {
                            int i2 = k % 16;
                            if (i2 != 0)
                                k -= i2;
                            i2 = m % 16;
                            if (i2 != 0)
                                m -= i2;
                            i2 = n % 16;
                            if (i2 != 0)
                                n = n - i2 + 16;
                            i2 = i1 % 16;
                            if (i2 != 0)
                                i1 = i1 - i2 + 16;
                            try {
                                System.out.println("" + k + ":" + m + ":" + n + ":" + i1 + ":" + (n - k) + ":" + (i1 - m));
                                Rect localRect = Rect.encode(this.pe, this.pf, paramBufferedImage.getSubimage(k, m, n - k, i1 - m), k, m);
                                return localRect;
                            } catch (Exception localException5) {
                                localException5.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
        return null;
    }

}
