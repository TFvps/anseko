package org.i9.slb.platform.anseko.libs.net.rfb;

import java.io.DataOutput;
import java.io.IOException;
import java.util.Vector;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb:
//			Rect, PixelFormat

public class Hextile extends Rect {
    public static class SubRect {

        public int pixel;
        public int x;
        public int y;
        public int w;
        public int h;

        public SubRect() {
        }
    }

    public static class Tile {

        public int bgpixel;
        public SubRect subrects[];

        public Tile() {
        }
    }

    public PixelFormat pixelFormat;
    public Object tiles[];

    public Hextile(int ai[], PixelFormat pixelformat, int i, int j, int k, int l, int i1,
                   int j1, int k1) {
        super(l, i1, j1, k1);
        pixelFormat = pixelformat;
        int ai1[] = copyPixels(ai, k, l - i, i1 - j, j1, k1);
        Vector vector = new Vector();
        int i3 = pixelformat.bitsPerPixel >> 3;
        int j3 = i3 << 8;
        for (int i2 = 0; i2 < k1; i2 += 16) {
            for (int l1 = 0; l1 < j1; l1 += 16) {
                int j2 = j1 - l1;
                if (j2 > 16)
                    j2 = 16;
                int k2 = k1 - i2;
                if (k2 > 16)
                    k2 = 16;
                Tile tile1 = tile(ai1, j1, l1, i2, j2, k2);
                int l2 = tile1.subrects.length * (2 + i3) + 2 * i3 + 1;
                if (l2 < j3)
                    vector.addElement(tile1);
                else
                    vector.addElement(raw(ai, k, i, j, l1 + l, i2 + i1, j2, k2));
            }

        }

        tiles = new Object[vector.size()];
        vector.toArray((Object[]) tiles);
    }

    public Hextile(int i, int j, int k, int l, PixelFormat pixelformat, Object aobj[]) {
        super(i, j, k, l);
        pixelFormat = pixelformat;
        tiles = aobj;
    }

    public void writeData(DataOutput dataoutput)
            throws IOException {
        super.writeData(dataoutput);
        dataoutput.writeInt(5);
        int j = 0x10000000;
        int k = 0x10000000;
        for (int k1 = 0; k1 < tiles.length; k1++)
            if (tiles[k1] instanceof Tile) {
                Tile tile1 = (Tile) tiles[k1];
                int i = 0;
                if (tile1.subrects.length > 0) {
                    i |= 8;
                    k = tile1.subrects[0].pixel;
                    int l = 1;
                    do {
                        if (l >= tile1.subrects.length)
                            break;
                        if (tile1.subrects[l].pixel != k) {
                            i |= 0x10;
                            break;
                        }
                        l++;
                    } while (true);
                    if ((i & 0x10) == 0)
                        i |= 4;
                }
                if (tile1.bgpixel != j) {
                    j = tile1.bgpixel;
                    i |= 2;
                }
                dataoutput.writeByte(i);
                if ((i & 2) != 0)
                    writePixel(dataoutput, pixelFormat, tile1.bgpixel);
                if ((i & 4) != 0)
                    writePixel(dataoutput, pixelFormat, k);
                if ((i & 8) == 0)
                    continue;
                dataoutput.writeByte(tile1.subrects.length);
                if ((i & 0x10) != 0) {
                    for (int i1 = 0; i1 < tile1.subrects.length; i1++) {
                        writePixel(dataoutput, pixelFormat, tile1.subrects[i1].pixel);
                        dataoutput.writeByte(tile1.subrects[i1].x << 4 | tile1.subrects[i1].y);
                        dataoutput.writeByte(tile1.subrects[i1].w - 1 << 4 | tile1.subrects[i1].h - 1);
                    }

                    continue;
                }
                for (int j1 = 0; j1 < tile1.subrects.length; j1++) {
                    dataoutput.writeByte(tile1.subrects[j1].x << 4 | tile1.subrects[j1].y);
                    dataoutput.writeByte(tile1.subrects[j1].w - 1 << 4 | tile1.subrects[j1].h - 1);
                }

            } else {
                dataoutput.writeByte(1);
                dataoutput.write((byte[]) tiles[k1]);
            }

    }

    public Object clone()
            throws CloneNotSupportedException {
        Object aobj[] = new Object[tiles.length];
        for (int j = 0; j < tiles.length; j++)
            if (tiles[j] instanceof Tile) {
                Tile tile2 = (Tile) tiles[j];
                Tile tile1 = new Tile();
                aobj[j] = tile1;
                tile1.bgpixel = tile2.bgpixel;
                tile1.subrects = new SubRect[tile2.subrects.length];
                for (int i = 0; i < tile2.subrects.length; i++) {
                    tile1.subrects[i] = new SubRect();
                    tile1.subrects[i].pixel = tile2.subrects[i].pixel;
                    tile1.subrects[i].x = tile2.subrects[i].x;
                    tile1.subrects[i].y = tile2.subrects[i].y;
                    tile1.subrects[i].w = tile2.subrects[i].w;
                    tile1.subrects[i].h = tile2.subrects[i].h;
                }

            } else {
                aobj[j] = ((byte[]) tiles[j]).clone();
            }

        return new Hextile(x, y, w, h, pixelFormat, aobj);
    }

    private Tile tile(int ai[], int i, int j, int k, int l, int i1) {
        Tile tile1 = new Tile();
        Vector vector = new Vector();
        int l2 = 0;
        int l3 = 0;
        tile1.bgpixel = getBackground(ai, i, j, k, l, i1);
        for (int l1 = 0; l1 < i1; l1++) {
            int i5 = (l1 + k) * i + j;
            label0:
            for (int k1 = 0; k1 < l; k1++) {
                if (ai[i5 + k1] == tile1.bgpixel)
                    continue;
                int j1 = ai[i5 + k1];
                int i3 = l1 - 1;
                boolean flag = true;
                int k2;
                for (k2 = l1; k2 < i1; k2++) {
                    int l4 = (k2 + k) * i + j;
                    if (ai[l4 + k1] != j1)
                        break;
                    int i2;
                    for (i2 = k1; i2 < l && ai[l4 + i2] == j1; i2++) ;
                    i2--;
                    if (k2 == l1)
                        l3 = l2 = i2;
                    if (i2 < l3)
                        l3 = i2;
                    if (flag && i2 >= l2)
                        i3++;
                    else
                        flag = false;
                }

                int i4 = k2 - 1;
                int j3 = (l2 - k1) + 1;
                int k3 = (i3 - l1) + 1;
                int j4 = (l3 - k1) + 1;
                int k4 = (i4 - l1) + 1;
                SubRect subrect = new SubRect();
                vector.addElement(subrect);
                subrect.pixel = j1;
                subrect.x = k1;
                subrect.y = l1;
                if (j3 * k3 > j4 * k4) {
                    subrect.w = j3;
                    subrect.h = k3;
                } else {
                    subrect.w = j4;
                    subrect.h = k4;
                }
                k2 = subrect.y;
                do {
                    if (k2 >= subrect.y + subrect.h)
                        continue label0;
                    for (int j2 = subrect.x; j2 < subrect.x + subrect.w; j2++)
                        ai[(k2 + k) * i + j + j2] = tile1.bgpixel;

                    k2++;
                } while (true);
            }

        }

        tile1.subrects = new SubRect[vector.size()];
        vector.toArray((Object[]) tile1.subrects);
        return tile1;
    }

    private byte[] raw(int ai[], int i, int j, int k, int l, int i1, int j1,
                       int k1) {
        int l1 = l - j;
        int i2 = i1 - k;
        byte abyte0[] = null;
        int j2 = 0;
        int k2 = 0;
        int l2 = 0;
        int k3 = j1 * k1;
        int l3 = i - j1;
        int i4 = i2 * i + l1;
        try {
            label0:
            switch (pixelFormat.bitsPerPixel) {
                default:
                    break;

                case 32: // ' '
                    abyte0 = new byte[k3 << 2];
                    while (k2 < k3) {
                        if (l2 == j1) {
                            l2 = 0;
                            i4 += l3;
                        }
                        int i3 = pixelFormat.translatePixel(ai[i4]);
                        abyte0[j2++] = (byte) (i3 & 0xff);
                        abyte0[j2++] = (byte) (i3 >> 8 & 0xff);
                        abyte0[j2++] = (byte) (i3 >> 16 & 0xff);
                        abyte0[j2++] = (byte) (i3 >> 24 & 0xff);
                        k2++;
                        l2++;
                        i4++;
                    }
                    break;

                case 16: // '\020'
                    abyte0 = new byte[k3 << 1];
                    do {
                        if (k2 >= k3)
                            break label0;
                        if (l2 == j1) {
                            l2 = 0;
                            i4 += l3;
                        }
                        int j3 = pixelFormat.translatePixel(ai[i4]);
                        abyte0[j2++] = (byte) (j3 & 0xff);
                        abyte0[j2++] = (byte) (j3 >> 8 & 0xff);
                        k2++;
                        l2++;
                        i4++;
                    } while (true);

                case 8: // '\b'
                    abyte0 = new byte[k3];
                    do {
                        if (k2 >= k3)
                            break label0;
                        if (l2 == j1) {
                            l2 = 0;
                            i4 += l3;
                        }
                        abyte0[k2] = (byte) pixelFormat.translatePixel(ai[i4]);
                        k2++;
                        l2++;
                        i4++;
                    } while (true);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
            System.out.println("Raw:scanline=" + i + ":jump=" + l3 + ":p=" + i4 + ":pl=" + ai.length + ":" + l1 + ":" + i2 + ":" + j1 + ":" + k1);
        }
        return abyte0;
    }
}
