package org.i9.slb.platform.anseko.libs.net.rfb.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb.server:
//			RFBAuthenticator, RFBSocket, RFBClient

public class NoAuthenticator
        implements RFBAuthenticator {

    public NoAuthenticator() {
    }

    public boolean authenticate(DataInputStream datainputstream, DataOutputStream dataoutputstream, RFBSocket rfbsocket)
            throws IOException {
        System.out.println("Starting authenication for NoAuthenicator: ");
        dataoutputstream.writeInt(1);
        dataoutputstream.flush();
        return true;
    }

    public int getAuthScheme(RFBClient rfbclient) {
        return 1;
    }
}
