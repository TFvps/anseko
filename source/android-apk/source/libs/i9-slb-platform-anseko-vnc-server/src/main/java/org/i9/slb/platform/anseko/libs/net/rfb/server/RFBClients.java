package org.i9.slb.platform.anseko.libs.net.rfb.server;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb.server:
//			RFBClient

public class RFBClients {

    Hashtable clients;

    public RFBClients() {
        clients = new Hashtable();
    }

    public boolean isEmpty() {
        return clients.isEmpty();
    }

    public void addClient(RFBClient rfbclient) {
        clients.put(rfbclient, new Hashtable());
    }

    public void removeClient(RFBClient rfbclient) {
        clients.remove(rfbclient);
    }

    public void closeAll() {
        for (Enumeration enumeration = elements(); enumeration.hasMoreElements(); ) {
            RFBClient rfbclient = (RFBClient) enumeration.nextElement();
            try {
                rfbclient.close();
            } catch (IOException ioexception) {
            }
        }

        clients.clear();
    }

    public Enumeration elements() {
        return clients.keys();
    }

    public void setProperty(RFBClient rfbclient, String s, Object obj) {
        Hashtable hashtable = (Hashtable) clients.get(rfbclient);
        if (hashtable == null) {
            return;
        } else {
            hashtable.put(s, obj);
            return;
        }
    }

    public Object getProperty(RFBClient rfbclient, String s) {
        Hashtable hashtable = (Hashtable) clients.get(rfbclient);
        if (hashtable == null)
            return null;
        else
            return hashtable.get(s);
    }
}
