package org.i9.slb.platform.anseko.libs.net.rfb;

import java.io.DataOutput;
import java.io.IOException;

// Referenced classes of package org.i9.slb.platform.anseko.libs.net.rfb:
//			Rect, PixelFormat

public class Raw extends Rect {

    public PixelFormat pixelFormat;
    public byte bytes[];

    public Raw(int ai[], PixelFormat pixelformat, int i, int j, int k, int l, int i1,
               int j1, int k1) {
        super(l, i1, j1, k1);
        pixelFormat = pixelformat;
        int l1 = 0;
        int i2 = 0;
        int j2 = 0;
        int l2 = j1 * k1;
        int i3 = k - j1;
        int j3 = ((i1 - j) * k + l) - i;
        switch (pixelformat.bitsPerPixel) {
            case 32: // ' '
                bytes = new byte[l2 << 2];
                break;

            case 24: // '\030'
                bytes = new byte[l2 * 3];
                break;

            case 16: // '\020'
                bytes = new byte[l2 << 1];
                break;

            case 8: // '\b'
                bytes = new byte[l2];
                break;
        }
        while (i2 < l2) {
            if (j2 == j1) {
                j2 = 0;
                j3 += i3;
            }
            int k2 = pixelformat.translatePixel(ai[j3]);
            byte byte0 = (byte) (k2 & 0xff);
            byte byte1 = (byte) (k2 >> 8 & 0xff);
            byte byte2 = (byte) (k2 >> 16 & 0xff);
            byte byte3 = (byte) (k2 >> 24 & 0xff);
            if (k2 != 0)
                System.out.print(k2 + " ");
            switch (pixelformat.bitsPerPixel) {
                case 32: // ' '
                    bytes[l1++] = byte0;
                    bytes[l1++] = byte1;
                    bytes[l1++] = byte2;
                    bytes[l1++] = byte3;
                    break;

                case 24: // '\030'
                    bytes[l1++] = byte0;
                    bytes[l1++] = byte1;
                    bytes[l1++] = byte2;
                    break;

                case 16: // '\020'
                    bytes[l1++] = byte0;
                    bytes[l1++] = byte1;
                    break;

                case 8: // '\b'
                    bytes[l1++] = byte0;
                    break;
            }
            i2++;
            j2++;
            j3++;
        }
    }

    public Raw(int i, int j, int k, int l, PixelFormat pixelformat, byte abyte0[]) {
        super(i, j, k, l);
        pixelFormat = pixelformat;
        bytes = abyte0;
    }

    public void writeData(DataOutput dataoutput)
            throws IOException {
        super.writeData(dataoutput);
        dataoutput.writeInt(0);
        dataoutput.write(bytes);
    }

    public Object clone()
            throws CloneNotSupportedException {
        return new Raw(x, y, w, h, pixelFormat, (byte[]) bytes.clone());
    }
}
