package org.i9.slb.platform.anseko.provider.repository.querybean;

public class UserQuery extends BasePageQuery implements java.io.Serializable {

    private static final long serialVersionUID = -5450701406426743642L;

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
