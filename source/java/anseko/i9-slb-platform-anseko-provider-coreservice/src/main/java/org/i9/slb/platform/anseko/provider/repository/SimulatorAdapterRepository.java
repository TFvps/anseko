package org.i9.slb.platform.anseko.provider.repository;

import org.i9.slb.platform.anseko.provider.entity.SimulatorAdapterEntity;

import java.util.List;

public interface SimulatorAdapterRepository {

    void insertSimulatorAdapter(SimulatorAdapterEntity simulatorAdapterEntity);

    List<SimulatorAdapterEntity> getSimulatorAdapterEntityList(String simulatorId);
}
