package org.i9.slb.platform.anseko.provider.repository;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.i9.slb.platform.anseko.provider.entity.InstanceEntity;
import org.i9.slb.platform.anseko.provider.repository.querybean.InstanceQuery;

import java.util.List;

/**
 * 实例dao
 *
 * @author R12
 * @version 1.0
 * @date 2018/9/4 10:48
 */
public interface InstanceRepository {

    /**
     * 获取实例信息
     *
     * @param id
     * @return
     */
    InstanceEntity getInstanceEntityById(String id);

    /**
     * 获取实例列表
     *
     * @return
     */
    List<InstanceEntity> getInstanceEntityList();

    /**
     * 保存实例信息
     *
     * @param instanceEntity
     */
    void insertInstanceEntity(InstanceEntity instanceEntity);

    /**
     * 查询实例名称是否存在
     *
     * @param instanceName
     * @return
     */
    @Select("SELECT COUNT(1) FROM tb_instance WHERE instanceName = #{instanceName}")
    int getCountInstanceEntityByInstanceNameNumber(@Param("instanceName") String instanceName);

    /**
     * 查询实例远程地址是否存在
     *
     * @param remoteAddress
     * @return
     */
    @Select("SELECT COUNT(1) FROM tb_instance WHERE remoteAddress = #{remoteAddress}")
    int getCountInstanceEntityByRemoteAddressNumber(@Param("remoteAddress") String remoteAddress);

    /**
     * 更新实例信息
     *
     * @param instanceEntity
     */
    void updateInstanceEntity(InstanceEntity instanceEntity);

    /**
     * 实例列表分页
     *
     * @param instanceQuery
     * @return
     */
    List<InstanceEntity> getInstanceEntityListPage(InstanceQuery instanceQuery);

    @Select("SELECT id, instanceName, virtualType, remoteAddress, createDate, updateDate, status FROM tb_instance WHERE status = 1")
    List<InstanceEntity> getInstanceEntityListOnline();
}
