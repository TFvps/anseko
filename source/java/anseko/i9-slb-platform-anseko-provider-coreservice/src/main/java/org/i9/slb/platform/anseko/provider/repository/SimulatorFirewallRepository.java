package org.i9.slb.platform.anseko.provider.repository;

import org.i9.slb.platform.anseko.provider.entity.SimulatorFirewallEntity;

import java.util.List;

public interface SimulatorFirewallRepository {

    void insertSimulatorFirewall(SimulatorFirewallEntity simulatorFirewallEntity);

    List<SimulatorFirewallEntity> getSimulatorFirewallEntityList(String simulatorId);
}
