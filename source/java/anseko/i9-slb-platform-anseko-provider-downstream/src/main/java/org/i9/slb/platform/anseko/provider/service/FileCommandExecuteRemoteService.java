package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.downstream.IFileCommandExecuteRemoteService;
import org.i9.slb.platform.anseko.downstream.dto.param.FileCommandParamDto;
import org.i9.slb.platform.anseko.provider.task.FileCommandExecuteBatchTask;
import org.i9.slb.platform.anseko.provider.task.FileCommandExecuteTask;
import org.i9.slb.platform.anseko.provider.task.pool.CommandExecuteSchedulerPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * file命令远程服务调用
 *
 * @author R12
 * @date 2018.08.29
 */
@Service("fileCommandExecuteRemoteService")
public class FileCommandExecuteRemoteService implements IFileCommandExecuteRemoteService {

    @Autowired
    private CommandExecuteSchedulerPool pool;

    /**
     * 执行单条文件命令
     *
     * @param clientId
     * @param fileCommandParamDto
     */
    @Override
    public void fileCommandExecute(String clientId, FileCommandParamDto fileCommandParamDto) {
        FileCommandExecuteTask fileCommandExecuteTask = new FileCommandExecuteTask(fileCommandParamDto);
        pool.submit(fileCommandExecuteTask);
    }

    /**
     * 批量执行文件命令
     *
     * @param clientId
     * @param fileCommandParamDtos
     */
    @Override
    public void fileCommandExecuteBatch(String clientId, List<FileCommandParamDto> fileCommandParamDtos) {
        FileCommandExecuteBatchTask fileCommandExecuteBatchTask = new FileCommandExecuteBatchTask(fileCommandParamDtos);
        pool.submit(fileCommandExecuteBatchTask);
    }

}
