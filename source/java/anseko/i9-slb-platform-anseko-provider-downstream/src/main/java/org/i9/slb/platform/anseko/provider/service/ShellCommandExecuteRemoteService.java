package org.i9.slb.platform.anseko.provider.service;

import org.i9.slb.platform.anseko.downstream.IShellCommandExecuteRemoteService;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.i9.slb.platform.anseko.provider.task.ShellCommandExecuteBatchTask;
import org.i9.slb.platform.anseko.provider.task.ShellCommandExecuteTask;
import org.i9.slb.platform.anseko.provider.task.pool.CommandExecuteSchedulerPool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 命令执行远程调用服务
 *
 * @author R12
 * @date 2018.08.28
 */
@Service("shellCommandExecuteRemoteService")
public class ShellCommandExecuteRemoteService implements IShellCommandExecuteRemoteService {

    @Autowired
    private CommandExecuteSchedulerPool pool;

    /**
     * 执行单条shell命令接口
     *
     * @param clientId
     * @param shellCommandParamDto
     */
    @Override
    public void shellCommandExecute(String clientId, ShellCommandParamDto shellCommandParamDto) {
        ShellCommandExecuteTask shellCommandExecuteTask = new ShellCommandExecuteTask(shellCommandParamDto);
        pool.submit(shellCommandExecuteTask);
    }

    /**
     * 批量执行shell命令接口
     *
     * @param clientId
     * @param shellCommandParamDtos
     */
    @Override
    public void shellCommandExecuteBatch(String clientId, List<ShellCommandParamDto> shellCommandParamDtos) {
        ShellCommandExecuteBatchTask shellCommandExecuteBatchTask = new ShellCommandExecuteBatchTask(shellCommandParamDtos);
        pool.submit(shellCommandExecuteBatchTask);
    }
}
