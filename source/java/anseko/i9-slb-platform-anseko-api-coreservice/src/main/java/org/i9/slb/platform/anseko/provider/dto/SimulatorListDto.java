package org.i9.slb.platform.anseko.provider.dto;

import java.util.List;

/**
 * 模拟器dto
 *
 * @author R12
 * @date 2018年9月4日 10:35:25
 */
public class SimulatorListDto implements java.io.Serializable {

    private static final long serialVersionUID = -603090465922577355L;

    private List<SimulatorDto> list;

    private int totalRow;

    public List<SimulatorDto> getList() {
        return list;
    }

    public void setList(List<SimulatorDto> list) {
        this.list = list;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
