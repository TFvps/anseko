package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.provider.dto.CommandCallbackDto;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteDto;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteListDto;
import org.i9.slb.platform.anseko.provider.dto.CommandExecuteSearchDto;
import org.i9.slb.platform.anseko.provider.dto.CommandGroupDto;
import org.i9.slb.platform.anseko.provider.dto.CommandGroupListDto;
import org.i9.slb.platform.anseko.provider.dto.CommandGroupSearchDto;

import java.util.List;

/**
 * 命令处理远程调用服务类
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:48
 */
public interface IDubboCommandRemoteService {
    /**
     * 发起一个命令调试指令
     *
     * @param commandGroupDto
     */
    DubboResult<?> launchCommandGroup(CommandGroupDto commandGroupDto);

    /**
     * 回调命令指令结果
     *
     * @param commandCallbackDto
     */
    DubboResult<?> callbackCommandExecute(CommandCallbackDto commandCallbackDto);

    /**
     * 获取模拟器执行命令组列表
     *
     * @param simulatorId
     * @return
     */
    DubboResult<List<CommandGroupDto>> getCommandGroupDtoListSimulatorId(final String simulatorId);

    /**
     * 获取模拟器执行命令列表
     *
     * @param commandGroupId
     * @return
     */
    DubboResult<List<CommandExecuteDto>> getCommandExecuteDtosCommandGroupId(final String commandGroupId);

    /**
     * 获取模拟器执行命令组列表分页
     *
     * @param commandGroupSearchDto
     * @return
     */
    DubboResult<CommandGroupListDto> getCommandGroupDtoListPage(CommandGroupSearchDto commandGroupSearchDto);

    /**
     * 获取模拟器执行命令列表分页
     *
     * @param commandExecuteSearchDto
     * @return
     */
    DubboResult<CommandExecuteListDto> getCommandExecuteListPage(CommandExecuteSearchDto commandExecuteSearchDto);

    /**
     * 获取模拟器执行命令详情
     *
     * @param commandId
     * @return
     */
    DubboResult<CommandExecuteDto> getCommandExecuteDto(String commandId);

    /**
     * 获取模拟器执行命令组详情
     *
     * @param commandGroupId
     * @return
     */
    DubboResult<CommandGroupDto> getCommandGroupDto(String commandGroupId);
}
