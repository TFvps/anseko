package org.i9.slb.platform.anseko.provider.dto;

/**
 * 命令执行器
 *
 * @author R12
 * @version 1.0
 * @date 2018/8/30 17:52
 */
public class CommandExecuteDto implements java.io.Serializable {

    private static final long serialVersionUID = -800863924642085848L;
    /**
     * 命令组编号
     */
    private String commandGroupId;
    /**
     * 执行位置
     */
    private Integer posIndex;
    /**
     * 命令编号
     */
    private String commandId;
    /**
     * 命令内容
     */
    private String commandLine;
    /**
     * 返回结果
     */
    private String commandResult;
    /**
     * 起始日期
     */
    private String startDate;
    /**
     * 结束日期
     */
    private String endDate;
    /**
     * 状态
     *
     * @see org.i9.slb.platform.anseko.common.constant.CommandExecuteStatusEnum
     */
    private Integer status;

    public String getCommandId() {
        return commandId;
    }

    public void setCommandId(String commandId) {
        this.commandId = commandId;
    }

    public String getCommandLine() {
        return commandLine;
    }

    public void setCommandLine(String commandLine) {
        this.commandLine = commandLine;
    }

    public String getCommandResult() {
        return commandResult;
    }

    public void setCommandResult(String commandResult) {
        this.commandResult = commandResult;
    }

    public String getCommandGroupId() {
        return commandGroupId;
    }

    public void setCommandGroupId(String commandGroupId) {
        this.commandGroupId = commandGroupId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Integer getPosIndex() {
        return posIndex;
    }

    public void setPosIndex(Integer posIndex) {
        this.posIndex = posIndex;
    }
}
