package org.i9.slb.platform.anseko.console.modules.user.service;

import org.i9.slb.platform.anseko.provider.dto.UserDto;

/**
 * 用户token缓存服务
 *
 * @author r12
 * @date 2019年2月12日 11:13:41
 */
public interface UserTokenRedisService {

    /**
     * 刷新用户token过期时间
     *
     * @param userToken
     */
    void refreshUserTokenExpire(String userToken);

    /**
     * 获取用户token值
     *
     * @param userToken
     * @return
     */
    String getUserTokenValue(String userToken);

    /**
     * 保存用户token值
     *
     * @param userToken
     * @param value
     */
    void saveUserTokenToRedisDb(String userToken, String value);

    /**
     * 创建用户token值，并保存在redis中
     *
     * @param userDto
     */
    String createUserTokenToRedisDb(UserDto userDto);

    void deleteUserToken(String userToken);
}
