package org.i9.slb.platform.anseko.console.modules.simulator.service.impl;

import org.i9.slb.platform.anseko.common.dubbo.DubboResult;
import org.i9.slb.platform.anseko.common.types.PowerStateEnum;
import org.i9.slb.platform.anseko.common.utils.UUIDUtil;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.SimulatorDetailsView;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.SimulatorInfoView;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.SimulatorListView;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.SimulatorSaveForm;
import org.i9.slb.platform.anseko.console.modules.simulator.bean.SimulatorSearch;
import org.i9.slb.platform.anseko.console.modules.simulator.service.SimulatorService;
import org.i9.slb.platform.anseko.console.utils.CoreserviceRemoteService;
import org.i9.slb.platform.anseko.console.utils.DownStreamRemoteCommand;
import org.i9.slb.platform.anseko.console.utils.SimulatorEnvironmentFactory;
import org.i9.slb.platform.anseko.downstream.dto.param.FileCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.GroupCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.i9.slb.platform.anseko.downstream.dto.param.SimpleCommandParamDto;
import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorBuilderHandle;
import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorHardwareHandle;
import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorNetworkPortHandle;
import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorOperateHandle;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorDisk;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorInfo;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorNetwork;
import org.i9.slb.platform.anseko.hypervisors.param.SimulatorViewer;
import org.i9.slb.platform.anseko.provider.IDubboSimulatorFirewallRemoteService;
import org.i9.slb.platform.anseko.provider.IDubboSimulatorRemoteService;
import org.i9.slb.platform.anseko.provider.dto.InstanceDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorAdapterDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDisplayDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorFirewallDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorInfoDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorListDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorSearchDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorStorageDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 模拟器操作服务类
 *
 * @author R12
 * @date 2018.08.31
 */
@Service("simulatorServiceImpl")
public class SimulatorServiceImpl implements SimulatorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SimulatorServiceImpl.class);

    @Autowired
    private CoreserviceRemoteService coreserviceRemoteService;

    @Autowired
    private DownStreamRemoteCommand downStreamRemoteCommand;

    @Autowired
    private IDubboSimulatorRemoteService dubboSimulatorRemoteService;

    @Autowired
    private IDubboSimulatorFirewallRemoteService dubboSimulatorFirewallRemoteService;

    @Autowired
    private SimulatorEnvironmentFactory simulatorEnvironmentFactory;

    /**
     * 创建模拟器
     *
     * @param simulatorSaveForm
     */
    @Override
    public void createSimulator(SimulatorSaveForm simulatorSaveForm) {
        String simulatorName = simulatorSaveForm.getSimulatorName() + ".idc9000.com";


        SimulatorCreateBuilder simulatorCreate = new SimulatorCreateBuilder(simulatorSaveForm, simulatorName,
                this.simulatorEnvironmentFactory.makeSimulatorHardwareHandle(simulatorName, simulatorSaveForm.getInstanceId())).invoke();
        SimulatorInfoDto simulatorInfoDto = simulatorCreate.getSimulatorInfoDto();
        this.buildSimulatorDefineXMLFileContent(simulatorInfoDto);
    }

    private void buildSimulatorDefineXMLFileContent(SimulatorInfoDto simulatorInfoDto) {
        List<SimpleCommandParamDto> commands = new ArrayList<SimpleCommandParamDto>();

        SimulatorDto simulatorDto = simulatorInfoDto.getSimulatorDto();
        SimulatorInfo simulatorInfo = new SimulatorInfo(simulatorDto.getSimulatorName(), simulatorDto.getCpunum(), simulatorDto.getRamnum(),
                simulatorDto.getInstanceId());

        SimulatorHardwareHandle simulatorHardwareHandle = this.simulatorEnvironmentFactory.makeSimulatorHardwareHandle(simulatorDto.getSimulatorName(), simulatorDto.getInstanceId());
        for (String commandLine : simulatorHardwareHandle.createDiskPath()) {
            ShellCommandParamDto shellCommandParamDto = ShellCommandParamDto.build(commandLine);
            commands.add(shellCommandParamDto);
        }

        // 生成模拟器远程显示服务
        SimulatorDisplayDto simulatorDisplayDto = simulatorInfoDto.getSimulatorDisplayDto();
        SimulatorViewer simulatorViewer = new SimulatorViewer(simulatorDisplayDto.getPort(), simulatorDisplayDto.getPassword());
        simulatorInfo.setSimulatorViewer(simulatorViewer);
        // 生成模拟器磁盘
        for (SimulatorDisk simulatorDisk : simulatorHardwareHandle.buildSimulatorDisks()) {
            simulatorInfo.getSimulatorDisks().add(simulatorDisk);
        }
        // 生成模拟器网卡
        for (SimulatorNetwork simulatorNetwork : simulatorHardwareHandle.buildSimulatorNetworks()) {
            simulatorInfo.getSimulatorNetworks().add(simulatorNetwork);
        }

        FileCommandParamDto fileCommandParamDto = new FileCommandParamDto();
        fileCommandParamDto.setCommandId(UUID.randomUUID().toString());
        SimulatorBuilderHandle simulatorBuilderHandle = simulatorEnvironmentFactory.makeSimulatorBuilderHandle(simulatorInfo);
        fileCommandParamDto.setFileContent(simulatorBuilderHandle.makeDefineSimulatorFile());
        fileCommandParamDto.setFilePath("/tmp/" + simulatorInfo.getName() + ".xml");
        fileCommandParamDto.setCommandLine("virsh define " + fileCommandParamDto.getFilePath());
        commands.add(fileCommandParamDto);

        // 执行远程调用服务
        this.downStreamRemoteCommand.multipleCommandExecute(simulatorDto.getId(), "构建模拟器", GroupCommandParamDto.build(commands));
    }

    /**
     * 关闭模拟器
     *
     * @param simulatorId
     */
    @Override
    public void shutdownSimulator(String simulatorId) {
        SimulatorDto simulatorDto = getRemoteServiceSimulatorDto(simulatorId);
        this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerStateEnum.CLOSE.ordinal());
        this.shutdownSimulator(simulatorDto);
    }

    /**
     * 启动模拟器
     *
     * @param simulatorId
     */
    @Override
    public void startSimulator(String simulatorId) {
        SimulatorDto simulatorDto = getRemoteServiceSimulatorDto(simulatorId);
        this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerStateEnum.RUNING.ordinal());
        this.startSimulator(simulatorDto);
    }

    /**
     * 重启模拟器
     *
     * @param simulatorId
     */
    @Override
    public void rebootSimulator(String simulatorId) {
        SimulatorDto simulatorDto = getRemoteServiceSimulatorDto(simulatorId);
        this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerStateEnum.RUNING.ordinal());
        this.rebootSimulator(simulatorDto);
    }

    private SimulatorDto getRemoteServiceSimulatorDto(String simulatorId) {
        DubboResult<SimulatorDto> dubboResult = this.dubboSimulatorRemoteService.getSimulatorDto(simulatorId);
        dubboResult.tryBusinessException();
        return dubboResult.getRe();
    }

    /**
     * 销毁模拟器
     *
     * @param simulatorId
     */
    @Override
    public void destroySimulator(String simulatorId) {
        SimulatorDto simulatorDto = getRemoteServiceSimulatorDto(simulatorId);
        // 更新模拟器状态为”关闭“
        this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorId, PowerStateEnum.CLOSE.ordinal());

        // 调用服务远程服务
        SimulatorOperateHandle simulatorOperateHandle = this.simulatorEnvironmentFactory.makeSimulatorOperateHandle(simulatorDto);
        simulatorOperateHandle.destroySimulator(simulatorDto.getSimulatorName());

        // 删除模拟器
        this.dubboSimulatorRemoteService.removeSimulatorInfo(simulatorId);
    }

    /**
     * 批量启动模拟器
     */
    @Override
    public void batchStartSimulator() {
        DubboResult<List<SimulatorDto>> dubboResult = this.dubboSimulatorRemoteService.getSimulatorDtos();
        dubboResult.tryBusinessException();
        for (SimulatorDto simulatorDto : dubboResult.getRe()) {
            try {
                this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerStateEnum.RUNING.ordinal());
                this.startSimulator(simulatorDto);
                LOGGER.info("批量启动模拟器 : {}, 成功", simulatorDto.getSimulatorName());
            } catch (Exception e) {
                LOGGER.error("批量启动模拟器 : {}, 失败", simulatorDto.getSimulatorName(), e);
            }
        }
    }

    /**
     * 批量重启模拟器
     */
    @Override
    public void batchRebootSimulator() {
        DubboResult<List<SimulatorDto>> dubboResult = this.dubboSimulatorRemoteService.getSimulatorDtos();
        dubboResult.tryBusinessException();
        for (SimulatorDto simulatorDto : dubboResult.getRe()) {
            this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerStateEnum.RUNING.ordinal());
            try {
                this.rebootSimulator(simulatorDto);
                LOGGER.info("批量重启模拟器 : {}, 成功", simulatorDto.getSimulatorName());
            } catch (Exception e) {
                LOGGER.error("批量重启模拟器 : {}, 失败", simulatorDto.getSimulatorName(), e);
            }
        }
    }

    /**
     * 批量关闭模拟器
     */
    @Override
    public void batchShutdownSimulator() {
        DubboResult<List<SimulatorDto>> dubboResult = this.dubboSimulatorRemoteService.getSimulatorDtos();
        dubboResult.tryBusinessException();
        for (SimulatorDto simulatorDto : dubboResult.getRe()) {
            this.dubboSimulatorRemoteService.refreshSimulatorPowerState(simulatorDto.getId(), PowerStateEnum.CLOSE.ordinal());
            try {
                this.shutdownSimulator(simulatorDto);
                LOGGER.info("批量关闭模拟器 : {}, 成功", simulatorDto.getSimulatorName());
            } catch (Exception e) {
                LOGGER.error("批量关闭模拟器 : {}, 失败", simulatorDto.getSimulatorName());
            }
        }
    }

    /**
     * 获取模拟器列表
     *
     * @return
     */
    @Override
    public List<SimulatorInfoView> getSimulatorList() {
        List<SimulatorInfoView> list = new ArrayList<SimulatorInfoView>();
        DubboResult<List<SimulatorDto>> dubboResult = this.dubboSimulatorRemoteService.getSimulatorDtos();
        dubboResult.tryBusinessException();
        for (SimulatorDto simulatorDto : dubboResult.getRe()) {
            SimulatorInfoView simulatorInfoView = new SimulatorInfoView();
            simulatorInfoView.setId(simulatorDto.getId());
            simulatorInfoView.setSimulatorName(simulatorDto.getSimulatorName());
            simulatorInfoView.setAndroidVersion(simulatorDto.getAndroidVersion());
            simulatorInfoView.setCpunum(simulatorDto.getCpunum());
            simulatorInfoView.setRamnum(simulatorDto.getRamnum());
            simulatorInfoView.setCreateDate(simulatorDto.getCreateDate());
            simulatorInfoView.setUpdateDate(simulatorDto.getUpdateDate());
            simulatorInfoView.setInstanceId(simulatorDto.getInstanceId());
            simulatorInfoView.setPowerStatus(simulatorDto.getPowerStatus());
            list.add(simulatorInfoView);
        }
        return list;
    }

    /**
     * 获取模拟器详情
     *
     * @param simulatorId
     * @return
     */
    @Override
    public SimulatorInfoView getSimulatorInfo(String simulatorId) {
        SimulatorDto simulatorDto = this.getRemoteServiceSimulatorDto(simulatorId);
        SimulatorInfoView simulatorInfoView = new SimulatorInfoView();
        simulatorInfoView.copyProperty(simulatorDto);
        return simulatorInfoView;
    }

    @Override
    public SimulatorListView getSimulatorListPage(SimulatorSearch simulatorSearch) {
        SimulatorSearchDto simulatorSearchDto = new SimulatorSearchDto();
        simulatorSearchDto.setSimulatorName(simulatorSearch.getSimulatorName());
        simulatorSearchDto.setInstanceId(simulatorSearch.getInstanceId());
        simulatorSearchDto.setPowerStatus(simulatorSearch.getPowerStatus());
        simulatorSearchDto.setStartPage(simulatorSearch.getStartPage());
        simulatorSearchDto.setPageSize(simulatorSearch.getPageSize());

        DubboResult<SimulatorListDto> dubboResult = this.dubboSimulatorRemoteService.getSimulatorDtosPage(simulatorSearchDto);
        dubboResult.tryBusinessException();

        SimulatorListDto simulatorListDto = dubboResult.getRe();
        SimulatorListView simulatorListView = new SimulatorListView();
        List<SimulatorInfoView> list = new ArrayList<SimulatorInfoView>();
        for (SimulatorDto simulatorDto : simulatorListDto.getList()) {
            SimulatorInfoView simulatorInfoView = new SimulatorInfoView();
            simulatorInfoView.copyProperty(simulatorDto);
            list.add(simulatorInfoView);
        }
        simulatorListView.setList(list);
        simulatorListView.setTotalRow(simulatorListDto.getTotalRow());
        return simulatorListView;
    }

    /**
     * 获取模拟器详情信息
     *
     * @param simulatorId
     * @return
     */
    @Override
    public SimulatorDetailsView getSimulatorDetailsInfo(String simulatorId) {
        DubboResult<SimulatorInfoDto> dubboResult = this.dubboSimulatorRemoteService.getSimulatorInfoDto(simulatorId);
        dubboResult.tryBusinessException();

        SimulatorDetailsView simulatorDetailsView = new SimulatorDetailsView();
        simulatorDetailsView.copyProperty(dubboResult.getRe());

        return simulatorDetailsView;
    }

    /**
     * 开启adb调试端口
     *
     * @param simulatorId
     */
    @Override
    public void debugPort(String simulatorId) {
        SimulatorDto simulatorDto = this.getRemoteServiceSimulatorDto(simulatorId);
        SimulatorNetworkPortHandle simulatorNetworkPortHandle = this.simulatorEnvironmentFactory.makeSimulatorNetworkPortHandle(simulatorDto);

        List<SimulatorFirewallDto> simulatorFirewallDtos = new ArrayList<SimulatorFirewallDto>();
        SimulatorFirewallDto simulatorFirewallDto = new SimulatorFirewallDto();
        simulatorFirewallDto.setSimulatorId(simulatorId);
        simulatorFirewallDto.setSport(5555);
        simulatorFirewallDto.setDport(5555);
        simulatorFirewallDto.setTypeStr("tcp");
        simulatorFirewallDto.setNameLabel("debugPort");
        simulatorFirewallDtos.add(simulatorFirewallDto);

        for (SimulatorFirewallDto simulatorFirewallDto0 : simulatorFirewallDtos) {
            simulatorNetworkPortHandle.executePortMappingCommand(simulatorFirewallDto0.getTypeStr(),
                    "", simulatorFirewallDto0.getSport(),
                    "", simulatorFirewallDto0.getDport());
            this.dubboSimulatorFirewallRemoteService.createSimulatorFirewall(simulatorFirewallDto0);
        }
    }

    private void shutdownSimulator(SimulatorDto simulatorDto) {
        SimulatorOperateHandle simulatorOperateHandle = this.simulatorEnvironmentFactory.makeSimulatorOperateHandle(simulatorDto);
        simulatorOperateHandle.shutdownSimulator(simulatorDto.getSimulatorName());
    }

    private void rebootSimulator(SimulatorDto simulatorDto) {
        SimulatorOperateHandle simulatorOperateHandle = this.simulatorEnvironmentFactory.makeSimulatorOperateHandle(simulatorDto);
        simulatorOperateHandle.rebootSimulatorCommand(simulatorDto.getSimulatorName());
    }

    private void startSimulator(SimulatorDto simulatorDto) {
        SimulatorOperateHandle simulatorOperateHandle = this.simulatorEnvironmentFactory.makeSimulatorOperateHandle(simulatorDto);
        simulatorOperateHandle.startSimulatorCommand(simulatorDto.getSimulatorName());
    }

    private class SimulatorCreateBuilder {

        private SimulatorSaveForm simulatorSaveForm;
        private SimulatorInfoDto simulatorInfoDto;
        private String simulatorName;
        private SimulatorHardwareHandle simulatorHardwareHandle;

        public SimulatorCreateBuilder(SimulatorSaveForm simulatorSaveForm, String simulatorName, SimulatorHardwareHandle simulatorHardwareHandle) {
            this.simulatorSaveForm = simulatorSaveForm;
            this.simulatorName = simulatorName;
            this.simulatorHardwareHandle = simulatorHardwareHandle;
        }

        public SimulatorCreateBuilder invoke() {
            InstanceDto instanceDto = coreserviceRemoteService.remoteServiceGetInstanceDtoInfo(simulatorSaveForm.getInstanceId());

            SimulatorDto simulatorDto = new SimulatorDto();
            simulatorDto.setId(UUIDUtil.generateUUID());
            simulatorDto.setSimulatorName(simulatorName);
            simulatorDto.setCpunum(simulatorSaveForm.getCpunum());
            simulatorDto.setRamnum(simulatorSaveForm.getRamnum());
            simulatorDto.setAndroidVersion(simulatorSaveForm.getAndroidVersion());
            simulatorDto.setInstanceId(instanceDto.getId());
            simulatorDto.setPowerStatus(PowerStateEnum.CLOSE.ordinal());

            int vncPort = dubboSimulatorRemoteService.getNextSimulatorVNCPortNumber(instanceDto.getId());

            simulatorInfoDto = new SimulatorInfoDto();
            simulatorInfoDto.setSimulatorDto(simulatorDto);
            for (SimulatorDisk simulatorDisk : this.simulatorHardwareHandle.buildSimulatorDisks()) {
                SimulatorStorageDto simulatorStorageDto = new SimulatorStorageDto();
                simulatorStorageDto.setSimulatorId(simulatorDto.getId());
                simulatorStorageDto.setStorageName("Storage-1");
                simulatorStorageDto.setStorageVolume(simulatorDisk.getDiskSize());
                simulatorStorageDto.setStoragePath(simulatorDisk.getDiskPath());
                simulatorInfoDto.getSimulatorStorageDtos().add(simulatorStorageDto);
            }
            for (SimulatorNetwork simulatorNetwork : this.simulatorHardwareHandle.buildSimulatorNetworks()) {
                SimulatorAdapterDto simulatorAdapterDto = new SimulatorAdapterDto();
                simulatorAdapterDto.setSimulatorId(simulatorDto.getId());
                simulatorAdapterDto.setAdapterName(simulatorNetwork.getTargetName());
                simulatorInfoDto.getSimulatorAdapterDtos().add(simulatorAdapterDto);
            }
            SimulatorFirewallDto simulatorFirewallDto = new SimulatorFirewallDto();
            simulatorFirewallDto.setSimulatorId(simulatorDto.getId());
            simulatorFirewallDto.setNameLabel("VNC");
            simulatorFirewallDto.setSport(vncPort);
            simulatorFirewallDto.setDport(vncPort);
            simulatorInfoDto.getSimulatorFirewallDtos().add(simulatorFirewallDto);

            SimulatorDisplayDto simulatorDisplayDto = new SimulatorDisplayDto();
            simulatorDisplayDto.setSimulatorId(simulatorDto.getId());
            simulatorDisplayDto.setPort(vncPort);
            simulatorDisplayDto.setPassword("i99999");
            simulatorInfoDto.setSimulatorDisplayDto(simulatorDisplayDto);

            DubboResult<?> dubboResult = dubboSimulatorRemoteService.createSimulatorInfo0(simulatorInfoDto);
            dubboResult.tryBusinessException();
            return this;
        }

        public SimulatorInfoDto getSimulatorInfoDto() {
            return this.simulatorInfoDto;
        }
    }
}
