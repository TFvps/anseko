package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import java.util.List;

/**
 * 命令执行器列表
 *
 * @author r12
 * @version 1.0
 * @date 2019/2/27 13:50
 */
public class CommandExecuteListView implements java.io.Serializable {

    private static final long serialVersionUID = -1514057215692125535L;

    private List<CommandExecuteInfoView> list;

    private int totalRow;

    public List<CommandExecuteInfoView> getList() {
        return list;
    }

    public void setList(List<CommandExecuteInfoView> list) {
        this.list = list;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }
}
