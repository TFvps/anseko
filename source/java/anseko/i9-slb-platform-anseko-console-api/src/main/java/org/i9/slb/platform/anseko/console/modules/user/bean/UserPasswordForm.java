package org.i9.slb.platform.anseko.console.modules.user.bean;

/**
 * 用户更新表单
 *
 * @author R12
 * @version 1.0
 * @date 2019/2/12 16:09
 */
public class UserPasswordForm implements java.io.Serializable {

    private static final long serialVersionUID = -6879107742333737210L;
    /**
     * 用户编号
     */
    private String id;
    /**
     * 密码
     */
    private String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
