package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import org.i9.slb.platform.anseko.provider.dto.SimulatorAdapterDto;

import java.util.Date;

public class SimulatorAdapterView implements java.io.Serializable {

    private static final long serialVersionUID = 8529921428600737271L;
    /**
     * 编号
     */
    private String id;
    /**
     * 模拟器编号
     */
    private String simulatorId;
    /**
     * 适配器名称
     */
    private String adapterName;
    /**
     * 创建日期
     */
    private Date createDate;
    /**
     * 更新日期
     */
    private Date updateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }

    public String getAdapterName() {
        return adapterName;
    }

    public void setAdapterName(String adapterName) {
        this.adapterName = adapterName;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public void copyProperty(SimulatorAdapterDto simulatorAdapterDto) {
        this.id = simulatorAdapterDto.getId();
        this.simulatorId = simulatorAdapterDto.getSimulatorId();
        this.createDate = simulatorAdapterDto.getCreateDate();
        this.updateDate = simulatorAdapterDto.getUpdateDate();
        this.adapterName = simulatorAdapterDto.getAdapterName();
    }
}
