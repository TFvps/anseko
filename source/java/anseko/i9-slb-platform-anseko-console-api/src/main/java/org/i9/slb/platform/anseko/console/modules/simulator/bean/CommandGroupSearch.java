package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import org.i9.slb.platform.anseko.console.utils.BaseSearch;

/**
 * 模拟器命令组搜索
 *
 * @author r12
 * @date 2019年2月27日 13:52:41
 */
public class CommandGroupSearch extends BaseSearch implements java.io.Serializable {

    private static final long serialVersionUID = -538207830010699480L;

    private String simulatorId;

    public String getSimulatorId() {
        return simulatorId;
    }

    public void setSimulatorId(String simulatorId) {
        this.simulatorId = simulatorId;
    }
}
