package org.i9.slb.platform.anseko.console.modules.simulator.service;

import org.i9.slb.platform.anseko.console.modules.simulator.bean.*;

import java.util.List;

/**
 * 模拟器操作服务类
 *
 * @author R12
 * @date 2018.08.31
 */
public interface SimulatorService {

    /**
     * 创建模拟器
     *
     * @param simulatorSaveForm
     */
    void createSimulator(SimulatorSaveForm simulatorSaveForm);

    /**
     * 关闭模拟器
     *
     * @param simulatorId
     */
    void shutdownSimulator(String simulatorId);

    /**
     * 启动模拟器
     *
     * @param simulatorId
     */
    void startSimulator(String simulatorId);

    /**
     * 重启模拟器
     *
     * @param simulatorId
     */
    void rebootSimulator(String simulatorId);

    /**
     * 销毁模拟器
     *
     * @param simulatorId
     */
    void destroySimulator(String simulatorId);

    /**
     * 批量启动模拟器
     */
    void batchStartSimulator();

    /**
     * 批量重启模拟器
     */
    void batchRebootSimulator();

    /**
     * 批量关闭模拟器
     */
    void batchShutdownSimulator();

    /**
     * 获取模拟器列表
     *
     * @return
     */
    List<SimulatorInfoView> getSimulatorList();

    /**
     * 获取模拟器详情
     *
     * @param simulatorId
     * @return
     */
    SimulatorInfoView getSimulatorInfo(String simulatorId);

    /**
     * 获取模拟器列表分页
     *
     * @param simulatorSearch
     * @return
     */
    SimulatorListView getSimulatorListPage(SimulatorSearch simulatorSearch);

    /**
     * 获取模拟器详情信息
     *
     * @param simulatorId
     * @return
     */
    SimulatorDetailsView getSimulatorDetailsInfo(String simulatorId);

    /**
     * 开启adb调试端口
     *
     * @param simulatorId
     */
    void debugPort(String simulatorId);
}
