package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import org.i9.slb.platform.anseko.common.types.AndroidVersionEnum;
import org.i9.slb.platform.anseko.common.types.PowerStateEnum;
import org.i9.slb.platform.anseko.provider.dto.SimulatorDto;

public class SimulatorInfoView implements java.io.Serializable {

    public String getPowerStatusDesc() {
        PowerStateEnum powerStateEnum = PowerStateEnum.valueOf(this.powerStatus);
        return powerStateEnum.getName();
    }

    public String getAndroidVersionDesc() {
        AndroidVersionEnum androidVersionEnum = AndroidVersionEnum.valueOf(this.androidVersion);
        return androidVersionEnum.getName();
    }

    private static final long serialVersionUID = 6044782533290097605L;
    /**
     * 模拟器编号
     */
    private String id;
    /**
     * 模拟器名称
     */
    private String simulatorName;
    /**
     * 创建日期
     */
    private String createDate;
    /**
     * 更新日期
     */
    private String updateDate;
    /**
     * cpu核数
     */
    private Integer cpunum;
    /**
     * 内存
     */
    private Integer ramnum;
    /**
     * 电源状态
     */
    private Integer powerStatus;
    /**
     * 安卓版本号
     */
    private Integer androidVersion;
    /**
     * 实例编号
     */
    private String instanceId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSimulatorName() {
        return simulatorName;
    }

    public void setSimulatorName(String simulatorName) {
        this.simulatorName = simulatorName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getCpunum() {
        return cpunum;
    }

    public void setCpunum(Integer cpunum) {
        this.cpunum = cpunum;
    }

    public Integer getRamnum() {
        return ramnum;
    }

    public void setRamnum(Integer ramnum) {
        this.ramnum = ramnum;
    }

    public Integer getPowerStatus() {
        return powerStatus;
    }

    public void setPowerStatus(Integer powerStatus) {
        this.powerStatus = powerStatus;
    }

    public Integer getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(Integer androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public void copyProperty(SimulatorDto simulatorDto) {
        this.setId(simulatorDto.getId());
        this.setSimulatorName(simulatorDto.getSimulatorName());
        this.setAndroidVersion(simulatorDto.getAndroidVersion());
        this.setCpunum(simulatorDto.getCpunum());
        this.setRamnum(simulatorDto.getRamnum());
        this.setCreateDate(simulatorDto.getCreateDate());
        this.setUpdateDate(simulatorDto.getUpdateDate());
        this.setInstanceId(simulatorDto.getInstanceId());
        this.setPowerStatus(simulatorDto.getPowerStatus());
    }
}
