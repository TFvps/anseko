package org.i9.slb.platform.anseko.console.modules.simulator.bean;

import org.i9.slb.platform.anseko.provider.dto.SimulatorAdapterDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorFirewallDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorInfoDto;
import org.i9.slb.platform.anseko.provider.dto.SimulatorStorageDto;

import java.util.ArrayList;
import java.util.List;

public class SimulatorDetailsView implements java.io.Serializable {

    private static final long serialVersionUID = 5093006106862664096L;

    private SimulatorInfoView simulatorInfoView;

    private SimulatorDisplayView simulatorDisplayView;

    private List<SimulatorAdapterView> simulatorAdapterViews;

    private List<SimulatorStorageView> simulatorStorageViews;

    private List<SimulatorFirewallView> simulatorFirewallViews;

    public SimulatorInfoView getSimulatorInfoView() {
        return simulatorInfoView;
    }

    public void setSimulatorInfoView(SimulatorInfoView simulatorInfoView) {
        this.simulatorInfoView = simulatorInfoView;
    }

    public SimulatorDisplayView getSimulatorDisplayView() {
        return simulatorDisplayView;
    }

    public void setSimulatorDisplayView(SimulatorDisplayView simulatorDisplayView) {
        this.simulatorDisplayView = simulatorDisplayView;
    }

    public List<SimulatorAdapterView> getSimulatorAdapterViews() {
        return simulatorAdapterViews;
    }

    public void setSimulatorAdapterViews(List<SimulatorAdapterView> simulatorAdapterViews) {
        this.simulatorAdapterViews = simulatorAdapterViews;
    }

    public List<SimulatorStorageView> getSimulatorStorageViews() {
        return simulatorStorageViews;
    }

    public void setSimulatorStorageViews(List<SimulatorStorageView> simulatorStorageViews) {
        this.simulatorStorageViews = simulatorStorageViews;
    }

    public List<SimulatorFirewallView> getSimulatorFirewallViews() {
        return simulatorFirewallViews;
    }

    public void setSimulatorFirewallViews(List<SimulatorFirewallView> simulatorFirewallViews) {
        this.simulatorFirewallViews = simulatorFirewallViews;
    }

    public void copyProperty(SimulatorInfoDto simulatorInfoDto) {
        this.simulatorInfoView = new SimulatorInfoView();
        this.simulatorInfoView.copyProperty(simulatorInfoDto.getSimulatorDto());
        this.simulatorAdapterViews = new ArrayList<SimulatorAdapterView>();
        for (SimulatorAdapterDto simulatorAdapterDto : simulatorInfoDto.getSimulatorAdapterDtos()) {
            SimulatorAdapterView simulatorAdapterView = new SimulatorAdapterView();
            simulatorAdapterView.copyProperty(simulatorAdapterDto);
            this.simulatorAdapterViews.add(simulatorAdapterView);
        }
        this.simulatorStorageViews = new ArrayList<SimulatorStorageView>();
        for (SimulatorStorageDto simulatorStorageDto : simulatorInfoDto.getSimulatorStorageDtos()) {
            SimulatorStorageView simulatorStorageView = new SimulatorStorageView();
            simulatorStorageView.copyProperty(simulatorStorageDto);
            simulatorStorageViews.add(simulatorStorageView);
        }
        this.simulatorFirewallViews = new ArrayList<SimulatorFirewallView>();
        for (SimulatorFirewallDto simulatorFirewallDto : simulatorInfoDto.getSimulatorFirewallDtos()) {
            SimulatorFirewallView simulatorFirewallView = new SimulatorFirewallView();
            simulatorFirewallView.copyProperty(simulatorFirewallDto);
            this.simulatorFirewallViews.add(simulatorFirewallView);
        }
        this.simulatorDisplayView = new SimulatorDisplayView();
        this.simulatorDisplayView.copyProperty(simulatorInfoDto.getSimulatorDisplayDto());
    }
}
