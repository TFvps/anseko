package org.i9.slb.platform.anseko.console.modules.common.bean;

public class EnumView implements java.io.Serializable {

    private static final long serialVersionUID = 7961580040857278702L;

    private int value;

    private String name;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
