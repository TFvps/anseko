package org.i9.slb.platform.anseko.console.modules.instance.bean;

import org.i9.slb.platform.anseko.console.utils.BaseSearch;

public class InstanceSearch extends BaseSearch implements java.io.Serializable {

    private static final long serialVersionUID = 712314664406417659L;

    private String instanceName;

    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }
}
