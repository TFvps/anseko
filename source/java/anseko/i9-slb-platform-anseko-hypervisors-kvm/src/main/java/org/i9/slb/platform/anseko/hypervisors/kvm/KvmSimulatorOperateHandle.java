package org.i9.slb.platform.anseko.hypervisors.kvm;

import org.i9.slb.platform.anseko.downstream.dto.param.ShellCommandParamDto;
import org.i9.slb.platform.anseko.hypervisors.handles.SimulatorOperateHandle;
import org.i9.slb.platform.anseko.hypervisors.service.IDownStreamRemoteCommand;

import java.util.ArrayList;
import java.util.List;

/**
 * 虚拟化硬件操作类
 *
 * @author tao.jiang03@ucarinc.com
 * @version 1.0
 * @date 2019/4/19 13:35
 */
public class KvmSimulatorOperateHandle extends SimulatorOperateHandle {
    /**
     * 虚拟化操作类构造函数
     *
     * @param simulatorId
     * @param downStreamRemoteCommand
     */
    public KvmSimulatorOperateHandle(String simulatorId, IDownStreamRemoteCommand downStreamRemoteCommand) {
        super(simulatorId, downStreamRemoteCommand);
    }

    /**
     * 启动模拟器
     *
     * @param simulatorName
     */
    @Override
    public void startSimulatorCommand(String simulatorName) {
        this.downStreamRemoteShellCommandExecute("启动模拟器", ShellCommandParamDto.build("virsh start " + simulatorName));
    }

    /**
     * 重启模拟器
     *
     * @param simulatorName
     */
    @Override
    public void rebootSimulatorCommand(String simulatorName) {
        List<ShellCommandParamDto> shellCommandParamDtos = new ArrayList<ShellCommandParamDto>();
        for (String command : new String[]{
                "virsh destroy " + simulatorName,
                "virsh start " + simulatorName
        }) {
            shellCommandParamDtos.add(ShellCommandParamDto.build(command));
        }
        this.downStreamRemoteShellCommandExecuteBatch("重启模拟器", shellCommandParamDtos);
    }

    /**
     * 关闭模拟器
     *
     * @param simulatorName
     */
    @Override
    public void shutdownSimulator(String simulatorName) {
        this.downStreamRemoteShellCommandExecute("关闭模拟器", ShellCommandParamDto.build("virsh destroy " + simulatorName));
    }

    /**
     * 取消定义模拟器命令
     *
     * @param simulatorName
     */
    @Override
    public void destroySimulator(String simulatorName) {
        List<ShellCommandParamDto> shellCommandParamDtos = new ArrayList<ShellCommandParamDto>();
        for (String command : new String[]{
                "virsh destroy " + simulatorName,
                "lvremove -f /dev/disk1/" + simulatorName,
                "virsh undefine " + simulatorName
        }) {
            shellCommandParamDtos.add(ShellCommandParamDto.build(command));
        }
        this.downStreamRemoteShellCommandExecuteBatch("销毁模拟器", shellCommandParamDtos);
    }
}
