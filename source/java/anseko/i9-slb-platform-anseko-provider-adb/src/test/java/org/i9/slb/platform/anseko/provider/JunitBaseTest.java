package org.i9.slb.platform.anseko.provider;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dubbo-provider.xml"})
public class JunitBaseTest {

    @Autowired
    protected IDubboAdbRemoteService dubboAdbRemoteService;

    @Before
    public void setUp() {
        this.dubboAdbRemoteService.startAdbServer();
    }

    @After
    public void after() {
        this.dubboAdbRemoteService.killAdbServer();
    }
}
