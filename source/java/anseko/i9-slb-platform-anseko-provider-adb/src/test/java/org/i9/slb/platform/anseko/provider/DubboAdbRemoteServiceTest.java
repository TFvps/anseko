package org.i9.slb.platform.anseko.provider;

import org.i9.slb.platform.anseko.provider.dto.AdbHostDto;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class DubboAdbRemoteServiceTest extends JunitBaseTest {

    @Test
    public void testGetAdbHostConnectStatus() {
        List<AdbHostDto> adbHostDtos = dubboAdbRemoteService.getAdbHostConnectStatus();
        System.out.println(adbHostDtos);
    }
}
