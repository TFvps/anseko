package org.i9.slb.platform.anseko.provider.utils;

import org.i9.slb.platform.anseko.common.exception.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;

/**
 * @author tao.jiang03@ucarinc.com
 * @version 1.0
 * @date 2019/3/25 12:45
 */
public class ShellUtils {

    public static String shellExecuteLocal(String remoteCommand) {
        try {
            StringWriter swriter = new StringWriter();
            Process process = exec(remoteCommand);
            process.waitFor();
            Reader reader = new InputStreamReader(process.getInputStream());
            char[] chars = new char[16];
            int read = -1;
            while ((read = reader.read(chars)) > -1) {
                swriter.write(chars, 0, read);
            }
            String executeResult = swriter.toString().trim();
            logger.info("execute:" + remoteCommand + " executeResult : " + executeResult);
            return executeResult;
        } catch (Exception e) {
            throw new BusinessException("执行命令失败", e);
        }
    }

    private static Process exec(String remoteCommand) throws IOException {
        Runtime runtime = Runtime.getRuntime();
        return runtime.exec(new String[]{"/bin/sh", "-c", remoteCommand});
    }

    private static final Logger logger = LoggerFactory.getLogger(ShellUtils.class);
}
