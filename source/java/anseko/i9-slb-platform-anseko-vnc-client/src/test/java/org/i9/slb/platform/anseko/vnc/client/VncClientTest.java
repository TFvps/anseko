package org.i9.slb.platform.anseko.vnc.client;

import org.i9.slb.platform.anseko.vnc.client.client.VncClient;
import org.i9.slb.platform.anseko.vnc.client.rfb.adapter.VncClientMessageHandlerAdapter;
import org.i9.slb.platform.anseko.vnc.client.rfb.adapter.develop.VncClientMessageHandlerConsoleAdapter;

import java.util.concurrent.Executors;

public class VncClientTest {

    public static void main(String[] argv) {
        final VncClientMessageHandlerAdapter adapter = new VncClientMessageHandlerConsoleAdapter();

        VncClient vncClient = VncClient.newInstance();
        vncClient.connect("61.181.128.231", 5999, "inasset.org", adapter);
        vncClient.processMessage();

        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                int index = 0;
                while (true) {
                    adapter.writeCharStrEvent("xiaobian" + (index ++), true);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                }
            }
        });
    }
}
