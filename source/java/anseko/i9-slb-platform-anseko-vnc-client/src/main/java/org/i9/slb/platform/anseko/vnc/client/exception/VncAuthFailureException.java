package org.i9.slb.platform.anseko.vnc.client.exception;

public class VncAuthFailureException extends RuntimeException {

    public VncAuthFailureException(String message) {
        super(message);
    }
}
