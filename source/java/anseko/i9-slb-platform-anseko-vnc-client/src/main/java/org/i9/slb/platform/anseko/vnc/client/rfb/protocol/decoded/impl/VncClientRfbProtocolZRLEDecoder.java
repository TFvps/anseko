package org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.impl;

import org.i9.slb.platform.anseko.vnc.client.exception.RfbProtocolException;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.RfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.impl.ZlibRfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.handler.VncClientMessageHandler;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.reader.VncClientMessageReader;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.VncClientRfbProtocolDecoder;

/**
 * vnc客户端rfb协议解码器（ZRLE）
 *
 * @author r12
 * @date 2019年2月1日 10:26:55
 */
public class VncClientRfbProtocolZRLEDecoder extends VncClientRfbProtocolDecoder {

    private VncClientMessageReader reader;

    private ZlibRfbInputStream inputStream;

    public VncClientRfbProtocolZRLEDecoder(VncClientMessageReader reader) {
        this.reader = reader;
        this.inputStream = new ZlibRfbInputStream();
    }

    static final int BPP = 8;

    static final int readPixel(RfbInputStream is) {
        return is.readU8();
    }

    @Override
    public void readRect(int x, int y, int w, int h, VncClientMessageHandler handler) {
        RfbInputStream is = reader.getInputStream();
        byte[] buf = reader.getImageBuf(64 * 64 * 4, 0);

        int length = is.readU32();
        inputStream.setUnderlying(is, length);

        for (int ty = y; ty < y + h; ty += 64) {

            int th = Math.min(y + h - ty, 64);

            for (int tx = x; tx < x + w; tx += 64) {

                int tw = Math.min(x + w - tx, 64);

                int mode = inputStream.readU8();
                boolean rle = (mode & 128) != 0;
                int palSize = mode & 127;
                int[] palette = new int[128];

                for (int i = 0; i < palSize; i++) {
                    palette[i] = readPixel(inputStream);
                }

                if (palSize == 1) {
                    int pix = palette[0];
                    handler.fillRect(tx, ty, tw, th, pix);
                    continue;
                }

                if (!rle) {
                    if (palSize == 0) {

                        // RAW

                        inputStream.readBytes(buf, 0, tw * th * (BPP / 8));

                    } else {

                        // packed pixels
                        int bppp = ((palSize > 16) ? 8 : ((palSize > 4) ? 4 : ((palSize > 2) ? 2 : 1)));

                        int ptr = 0;

                        for (int i = 0; i < th; i++) {
                            int eol = ptr + tw;
                            int b = 0;
                            int nbits = 0;

                            while (ptr < eol) {
                                if (nbits == 0) {
                                    b = inputStream.readU8();
                                    nbits = 8;
                                }
                                nbits -= bppp;
                                int index = (b >> nbits) & ((1 << bppp) - 1) & 127;
                                buf[ptr++] = (byte) palette[index];
                            }
                        }
                    }

                } else {

                    if (palSize == 0) {

                        // plain RLE

                        int ptr = 0;
                        int end = ptr + tw * th;
                        while (ptr < end) {
                            int pix = readPixel(inputStream);
                            int len = 1;
                            int b;
                            do {
                                b = inputStream.readU8();
                                len += b;
                            } while (b == 255);

                            if (!(len <= end - ptr))
                                throw new RfbProtocolException("ZRLEDecoder: assertion (len <= end - ptr)" + " failed");

                            while (len-- > 0)
                                buf[ptr++] = (byte) pix;
                        }
                    } else {

                        // palette RLE

                        int ptr = 0;
                        int end = ptr + tw * th;
                        while (ptr < end) {
                            int index = inputStream.readU8();
                            int len = 1;
                            if ((index & 128) != 0) {
                                int b;
                                do {
                                    b = inputStream.readU8();
                                    len += b;
                                } while (b == 255);

                                if (!(len <= end - ptr))
                                    throw new RfbProtocolException("ZRLEDecoder: assertion " + "(len <= end - ptr) failed");
                            }

                            index &= 127;

                            int pix = palette[index];

                            while (len-- > 0)
                                buf[ptr++] = (byte) pix;
                        }
                    }
                }

                handler.imageRect(tx, ty, tw, th, buf, 0);
            }
        }

        inputStream.reset();
    }
}
