package org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded;

import org.i9.slb.platform.anseko.vnc.client.rfb.constant.Encodings;
import org.i9.slb.platform.anseko.vnc.client.rfb.handler.VncClientMessageHandler;
import org.i9.slb.platform.anseko.vnc.client.rfb.message.reader.VncClientMessageReader;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.impl.VncClientRfbProtocolHextileDecoder;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.impl.VncClientRfbProtocolRREDecoder;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.impl.VncClientRfbProtocolRawDecoder;
import org.i9.slb.platform.anseko.vnc.client.rfb.protocol.decoded.impl.VncClientRfbProtocolZRLEDecoder;

/**
 * vnc客户端rfb协议解码器基类
 *
 * @author r12
 * @date 2019年2月1日 10:26:55
 */
public abstract class VncClientRfbProtocolDecoder {

    abstract public void readRect(int x, int y, int width, int height, VncClientMessageHandler handler);

    /**
     * 创建rfb协议解码器对象
     *
     * @param encoding
     * @param reader
     * @return
     */
    public static VncClientRfbProtocolDecoder createDecoder(int encoding, VncClientMessageReader reader) {
        switch (encoding) {
            case Encodings.RAW:
                return new VncClientRfbProtocolRawDecoder(reader);
            case Encodings.RRE:
                return new VncClientRfbProtocolRREDecoder(reader);
            case Encodings.HEXTILE:
                return new VncClientRfbProtocolHextileDecoder(reader);
            case Encodings.ZRLE:
                return new VncClientRfbProtocolZRLEDecoder(reader);
        }
        return null;
    }

    /**
     * 检查是否支持rfb协议解码器
     *
     * @param encoding
     * @return
     */
    public static boolean supported(int encoding) {
        return (encoding == Encodings.RAW || encoding == Encodings.RRE || encoding == Encodings.HEXTILE || encoding == Encodings.ZRLE);
    }
}
