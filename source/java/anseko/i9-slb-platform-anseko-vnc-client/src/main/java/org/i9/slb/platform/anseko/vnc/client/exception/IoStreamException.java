package org.i9.slb.platform.anseko.vnc.client.exception;

/**
 * io数据流异常
 *
 * @author jiangtao
 * @date 2019-01-22
 */
public class IoStreamException extends RuntimeException {

    private static final long serialVersionUID = -178104721526226061L;

    public IoStreamException(String message) {
        super(message);
    }
}
