package org.i9.slb.platform.anseko.vnc.client.client;

import org.apache.log4j.Logger;
import org.i9.slb.platform.anseko.vnc.client.exception.VncConnFailureException;
import org.i9.slb.platform.anseko.vnc.client.iostream.input.impl.JavaRfbInputStream;
import org.i9.slb.platform.anseko.vnc.client.iostream.output.impl.JavaRfbOutputStream;
import org.i9.slb.platform.anseko.vnc.client.rfb.adapter.VncClientMessageHandlerAdapter;

import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author jiangtao
 * @version 1.0
 * @date 2019/1/24 10:17
 */
public class VncClient {

    private static final Logger LOGGER = Logger.getLogger(VncClient.class);
    /**
     * socket服务
     */
    private Socket socket;
    /**
     * vnc适配器封装
     */
    private VncClientMessageHandlerAdapter adapter;

    private VncClient() {
    }

    /**
     * 创建新的实例
     *
     * @return
     */
    public static VncClient newInstance() {
        VncClient instance = new VncClient();
        return instance;
    }

    /**
     * 连接vnc服务器
     *
     * @param serverHost
     * @param serverPort
     */
    public void connect(String serverHost, int serverPort, String password, VncClientMessageHandlerAdapter adapter) {
        try {
            if (this.socket != null) {
                String hostname = serverHost + "::" + serverPort;
                throw new VncConnFailureException("当前已与VNC服务器连接, hostname : " + hostname);
            }
            try {
                this.socket = new Socket(serverHost, serverPort);
            } catch (IOException e) {
                String hostname = serverHost + "::" + serverPort;
                throw new VncConnFailureException("与vnc服务器建立连接失败, hostname : " + hostname);
            }
            this.adapter = adapter;
            this.init(password);
        } catch (VncConnFailureException e) {
            this.cleanServerConnectState();
            throw e;
        }
    }

    /**
     * 清除服务器连接状态
     */
    private void cleanServerConnectState() {
        if (this.socket != null) {
            try {
                this.socket.close();
            } catch (IOException e) {
            }
            this.socket = null;
        }
    }

    /**
     * 设置vnc连接服务器名称
     *
     * @param password
     */
    private void init(String password) {
        try {
            this.adapter.setServerName(this.socket.getInetAddress().getHostAddress() + "::" + this.socket.getPort());
            this.adapter.setRfbStreams(new JavaRfbInputStream(this.socket.getInputStream()), new JavaRfbOutputStream(this.socket.getOutputStream()));
            this.adapter.initialiseProtocol();
            this.adapter.getConnectionParameter().setPassword(password);
            LOGGER.info("vnc服务器连接成功, hostname : " + this.adapter.getServerName());
        } catch (Exception e) {
            throw new VncConnFailureException("与vnc服务器建立连接失败, hostname : " + this.adapter.getServerName());
        }
    }

    /**
     * 处理消息线程
     */
    public void processMessage() {
        ExecutorService singleThreadExecutor = Executors.newSingleThreadExecutor();
        singleThreadExecutor.execute(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    adapter.processMessage();
                }
            }
        });
    }

    /**
     * 获取消息处理适配器
     *
     * @return
     */
    public VncClientMessageHandlerAdapter getAdapter() {
        return adapter;
    }
}
