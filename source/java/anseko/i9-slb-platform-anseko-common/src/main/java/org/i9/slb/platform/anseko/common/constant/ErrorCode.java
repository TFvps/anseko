package org.i9.slb.platform.anseko.common.constant;

public class ErrorCode {
    /**
     * 成功
     */
    public static final Integer SUCCESS = 0;
    /**
     * 未知错误
     */
    public static final Integer UNKOWN_ERROR = 1;
    /**
     * 业务异常
     */
    public static final Integer BUSINESS_EXCEPTION = 2;
    /**
     * 用户未登录
     */
    public static final Integer NON_LOGIN = 3;
}
