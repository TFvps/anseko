import Cookies from 'js-cookie'

const tokenKey = 'Admin-Token'

export function getUserToken() {
    return Cookies.get(tokenKey)
}

export function saveUserToken(token) {
    return Cookies.set(tokenKey, token)
}

export function removeUserToken() {
    return Cookies.remove(tokenKey)
}
